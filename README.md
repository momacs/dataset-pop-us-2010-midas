# 2010 U.S. Synthesized Population Dataset (MIDAS Program; mirror)


## Download

To get all the state files at once simply clone the repository like so:

```
git clone https://gitlab.com/momacs/dataset-pop-us-2010-midas
```

To download individual files, use the following links (or head to the [`zip`](zip) directory):

| State                                                                       | Size [MB] |
|-----------------------------------------------------------------------------------|----:|
| [AK](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/AK.zip) |  10 |
| [AL](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/AL.zip) |  68 |
| [AR](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/AR.zip) |  41 |
| [AZ](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/AZ.zip) |  87 |
| [CA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/CA.zip) | 503 |
| [CO](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/CO.zip) |  74 |
| [CT](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/CT.zip) |  51 |
| [DC](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/DC.zip) |   9 |
| [DE](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/DE.zip) |  13 |
| [FL](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/FL.zip) | 267 |
| [GA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/GA.zip) | 139 |
| [HI](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/HI.zip) |  17 |
| [IA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/IA.zip) |  45 |
| [ID](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/ID.zip) |  22 |
| [IL](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/IL.zip) | 182 |
| [IN](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/IN.zip) |  92 |
| [KS](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/KS.zip) |  41 |
| [KY](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/KY.zip) |  62 |
| [LA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/LA.zip) |  64 |
| [MA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/MA.zip) |  95 |
| [MD](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/MD.zip) |  84 |
| [ME](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/ME.zip) |  20 |
| [MI](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/MI.zip) | 142 |
| [MN](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/MN.zip) |  78 |
| [MO](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/MO.zip) |  87 |
| [MS](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/MS.zip) |  41 |
| [MT](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/MT.zip) |  14 |
| [NC](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NC.zip) | 136 |
| [ND](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/ND.zip) |  10 |
| [NE](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NE.zip) |  27 |
| [NH](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NH.zip) |  20 |
| [NJ](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NJ.zip) | 127 |
| [NM](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NM.zip) |  28 |
| [NV](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NV.zip) |  37 |
| [NY](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NY.zip) | 272 |
| [NC](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NC.zip) |  54 |
| [OH](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/NC.zip) | 165 |
| [OK](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/OK.zip) |  54 |
| [OR](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/OR.zip) |  55 |
| [PA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/PA.zip) | 182 |
| [RI](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/RI.zip) |  15 |
| [SC](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/SC.zip) |  65 |
| [SD](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/SD.zip) |  12 |
| [TN](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/TN.zip) |  91 |
| [TX](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/TX.zip) | 346 |
| [UT](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/UT.zip) |  36 |
| [VA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/VA.zip) | 118 | 
| [VT](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/VT.zip) |   9 |
| [WA](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/WA.zip) |  96 |
| [WI](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/WI.zip) |  84 |
| [WV](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/WV.zip) |  27 |
| [WY](https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/zip/WY.zip) |   8 |

The data is also available at the [synthetic population page](https://fred.publichealth.pitt.edu/syn_pops) of the Public Health Dynamics Lab, University of Pittsburgh.  Head there to download specific county-level files.


## Contact

Bill Wheaton<br />
Director, Geospatial Science and Technology Program<br />
RTI International<br />
P.O. Box 12194<br />
3040 Cornwallis Rd.<br />
Research Triangle Park, NC 27709<br />
wdw@rti.org<br />
919-541-6158<br />


## Citation

NIGMS and RTI request that you cite these data in any publication or report in which they were used.  The proper citations for the data and the quick start guide are respectively:

RTI (2014). 2010 U.S. Synthetic Population Ver. 1.0. RTI International. Downloaded from https://gitlab.com/momacs/dataset-pop-us-2010-midas

Wheaton, W.D. (2014). 2010 U.S. Synthetic Population Quick Start Guide.  RTI International. Downloaded from https://gitlab.com/momacs/dataset-pop-us-2010-midas/-/raw/master/2010_synth_pop_ver1_quickstart.pdf


## Acknowledgments

The development of this data was supported by Grant Number U24GM0877044 (MIDAS) from the National Institutes of General Medical Sciences (NIGMS).

The content is solely the responsibility of the authors and does not necessarily represent the official views of the NIGMS or the National Institutes of Health.


## References and Resources

- [RTI U.S. Synthetic Household Population](https://www.rti.org/impact/rti-us-synthetic-household-population%E2%84%A2). RTI International.
- [Synthetic Population Viewer Web App](http://synthpopviewer.rti.org/). RTI International.
- [FRED (A Framework for Reconstructing Epidemiological Dynamics): Population Files](https://fred.publichealth.pitt.edu/syn_pops). Public Health Dynamics Lab, University of Pittsburgh.
- [Iterative Proportional Fitting](https://en.wikipedia.org/wiki/Iterative_proportional_fitting). Wikipedia.